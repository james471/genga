GENGA documentation
===================

| Author: Simon L. Grimm
| Center for Space and Habitability (CSH)
| University of Bern, Switzerland


GENGA is available at https://bitbucket.org/sigrimm/genga


License
-------

GENGA is free to use, but when results from GENGA are published, then the following paper has to be referenced :cite:p:`Grimm+2014`.


Setup
-----

.. toctree::
   :maxdepth: 2
   :caption: Setup:

   Tutorial.rst
   Setup.rst
   Run.rst
   InitialConditions.rst
   Param.rst
   ConsoleArguments.rst
   Define.rst

Integrator details
------------------

.. toctree::
   :maxdepth: 2
   :caption: Integrator Details:

   Integrator.rst

Output Files
------------

.. toctree::
   :maxdepth: 2
   :caption: Output Files:

   Files.rst

Options
-------

.. toctree::
   :maxdepth: 2
   :caption: Options:

   Collisions.rst
   Encounters.rst
   Ejection.rst
   aeGrid.rst
   SetElements.rst
   KickFloat.rst
   SerialGrouping.rst
   Poincare.rst

Forces
------

.. toctree::
   :maxdepth: 2
   :caption: Forces:

   Gas.rst
   GR.rst
   Tides.rst
   RotationalDeformation.rst
   J2.rst
   Yarkovsky.rst
   PRdrag.rst

Small body collision model
--------------------------

.. toctree::
   :maxdepth: 2
   :caption: Small Body Collision Model:

   SmallBodies.rst



Bibliography
------------

.. toctree::
   :maxdepth: 2
   :caption: Bibliography:

   Bibliography.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
